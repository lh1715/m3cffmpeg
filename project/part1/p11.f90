!Module for solving n-d optimization problems with Newton's method and 2-d problems
!with bracket descent. Necessary cost function details are provided in separate cost
!module.
module part1
  use cost
  implicit none
  integer :: itermax = 100 !maximum number of iterations used by an optimizer
  real(kind=8) :: tol=1.0e-6 !stopping criteria for optimizer
  real(kind=8), allocatable :: jpath(:), xpath(:,:) !allocatable 1-d and 2-d arrays, should contain cost and location values used during optimization iterations
  real(kind=8), allocatable :: jpathf(:,:), xpathf(:,:,:)
  integer, allocatable, dimension(:) :: niter
  contains

  subroutine bracket_descent(xguess,xf,jf)
    !Use bracket descent method to minimize cost function, costj
    !input: xguess -- initial guess for loaction of minimum
    !output: xf -- computed location of minimum, jf -- computed minimum
    !Should also set module variables xpath and jpath appropriately
    !Assumes size(xguess) = 2
    implicit none
    real(kind=8), dimension(2), intent(in) :: xguess
    real(kind=8), intent(out) :: xf(2),jf !location of minimum, minimum cost
    integer :: i1,k1
    real(kind=8) :: j3(3),x3(3,2),j3temp(3),x3temp(3,2),xtemp(itermax+1,2),jtemp(itermax+1)
    real(kind=8) :: xm(2),dx(2),xr(2),jr,j_old
    logical :: convergence_flag

    !initialize triangle, costs
    call bd_initialize(xguess,x3,j3)
    call bd_sort(x3,j3,x3temp,j3temp)
    j3 = j3temp
    x3 = x3temp
    i1=0
    xtemp(i1+1,:) = sum(x3,dim=1)/3
    call costj(xtemp(i1+1,:),j_old)
    jtemp(i1+1) = j_old
    convergence_flag = .False.


    !Iterate using b-d scheme
    do while(i1<itermax .and. (.not. convergence_flag))
      !reflect
      xm = 0.5d0*(x3(1,:)+x3(2,:))
      dx = x3(3,:)-xm
      xf = xm-dx

      call costj(xf,jf)

      if (jf<j3(3)) then
        if (jf>j3(1)) then !accept new vertex
            j3(3) = jf
            x3(3,:) = xf
        else !extend

          xr = xm-2*dx
          call costj(xr,jr)
          if (jr<jf) then !accept extension
            j3(3) = jr
            x3(3,:) = xr
          else !accept original reflection
            j3(3) = jf
            x3(3,:) = xf
          end if
        end if
      else
        xf = xm - dx/2 !contract
        call costj(xf,jf)
        if (jf<j3(3)) then !accept contraction
          j3(3) = jf
          x3(3,:) = xf
        else !contract further
          xf = xm + dx/2
          call costj(xf,jf)
          if (jf<j3(3)) then !accept 2nd contraction
            j3(3) = jf
            x3(3,:) = xf
          else !shrink
            do k1=2,3
              x3(k1,:) = 0.5d0*(x3(1,:)+x3(k1,:))
              xf = x3(k1,:)
              call costj(xf,jf)
              j3(k1) = jf
            end do
          end if
        end if
      end if

      !compute centroid, sort vertices, check for convergence
      i1 = i1 + 1
      xtemp(i1+1,:) = sum(x3,dim=1)/3
      xf = xtemp(i1+1,:)
      call costj(xf,jf)
      jtemp(i1+1) = jf
      call bd_sort(x3,j3,x3temp,j3temp)
      x3 = x3temp
      j3 = j3temp
        call convergence_check(j_old,jf,convergence_flag)
      j_old = jf
    end do

    !set xpath, jpath
    if (allocated(xpath)) deallocate(xpath)
    if (allocated(jpath)) deallocate(jpath)
    allocate(xpath(i1,2),jpath(i1))
    xpath = xtemp(1:i1,:)
    jpath = jtemp(1:i1)
    xf = x3(1,:)
    jf = j3(1)
  end subroutine bracket_descent


  subroutine bdglobal(xguess,nb,d,alpha,xf,jf)
    !Use bracket descent method to minimize cost function, costj
    !input: xguess -- initial guess for location of minimum
    !nb: use nb * nb triangles
    !d: grid spacing for initial nb x nb grid of triangles
    !alpha: agressiveness in centroid shift
    !output: xf -- computed location of minimum, jf -- computed minimum
    !Should also set module variables xpath and jpath appropriately
    !Assumes size(xguess) = 2
    implicit none
    real(kind=8), dimension(2), intent(in) :: xguess
    integer, intent(in) :: nb
    real(kind=8), intent(in) :: d, alpha
    real(kind=8), intent(out) :: xf(2),jf !location of minimum, minimum cost
    integer :: i1,j1,k1
    real(kind=8), dimension(2,nb*nb) :: xg

    !Set up initial grid of triangles
    k1 = 0
    do i1 = 1,nb
      do j1=1,nb
        k1 = k1 + 1
        xg(1,k1) = (j1-1)*d - (nb-1)*d/2 + xguess(1)
        xg(2,k1) = (i1-1)*d - (nb-1)*d/2 + xguess(2)
      end do
    end do

  end subroutine bdglobal


  subroutine bd_initialize(xguess,x3,j3)
    !given xguess, generates vertices (x3) and corresponding costs (j3) for initial
    !bracket descent step
    implicit none
    real(kind=8), intent(in) :: xguess(2)
    real(kind=8), intent(out) :: j3(3),x3(3,2) !location of minimum
    integer :: i1
    real(kind=8), parameter :: l=1.d0

    x3(1,1) = xguess(1)
    x3(2,1) = xguess(1)+l*sqrt(3.d0)/2
    x3(3,1) = xguess(1)-l*sqrt(3.d0)/2
    x3(1,2) = xguess(2)+l
    x3(2,2) = xguess(2)-l/2
    x3(3,2) = xguess(2)-l/2

    do i1=1,3
      call costj(x3(i1,:),j3(i1))
    end do
  end subroutine bd_initialize

  subroutine bd_sort(x,c,xs,cs)
    !Three element insertion sort
    implicit none
    real(kind=8), intent(in) :: c(3),x(3,2)
    real(kind=8), intent(out) :: cs(3),xs(3,2)
    real(kind=8), dimension(2) :: temp

    cs = c
    xs = x
    if (c(2)<c(1)) then
      cs(1) = c(2)
      cs(2) = c(1)
      xs(1,:) = x(2,:)
      xs(2,:) = x(1,:)
    end if

    if (c(3)<cs(2)) then
      cs(3) = cs(2)
      cs(2) = c(3)
      xs(3,:) = xs(2,:)
      xs(2,:) = x(3,:)

      if (c(3)<cs(1)) then
        temp(1) = cs(1)
        cs(1) = cs(2)
        cs(2) = temp(1)
        temp = xs(1,:)
        xs(1,:) = xs(2,:)
        xs(2,:) = temp
      end if
    end if

  end subroutine bd_sort


  subroutine convergence_check(j1,j2,flag_converged)
    !check if costs j1 and j2 satisfy convergence criteria
    implicit none
    real(kind=8), intent(in) :: j1,j2
    real(kind=8) :: test
    logical, intent(out) :: flag_converged

    test = abs(j1-j2)/max(abs(j1),abs(j2),1.d0)
    if (test .le. tol) then
      flag_converged = .True.
    else
      flag_converged = .False.
    end if
  end subroutine convergence_check


end module part1
