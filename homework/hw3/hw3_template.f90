module hw3
  use omp_lib
  implicit none
  contains

    subroutine rwnet(h,dim,n,display,x)
      !Simulate random network model
      !Input variables
      !h: Height at which nodes are initially introduced
      !n: Total number of nodes
      !dim: dimension of node coordinates
      !display: Coordinate of every 100th node added to network is printed to screen when display=1
      !Output variables
      !x: Final node coordinates

      implicit none
      integer, intent(in) :: h,dim,n,display
      integer, dimension(dim,n), intent(out) ::  x
      integer :: i1,j1,deltaX,dist
      integer, dimension(dim) :: r,xx

      !initialize
      x = 0

      do i1 = 2,n
        xx(1:dim-1) = 0 !introduce new node
        xx(dim) = h
        dist=h
        do j1=1,i1-1
          deltaX = maxval(abs(x(:,j1)-xx))
          dist = min(dist,deltaX)
        end do

        do while ((xx(dim)>0) .and. (dist>1))
          call random_int(dim,r)
          r(1:dim-1) = r(1:dim-1)*2-1
          r(dim) = -r(dim)
          xx = xx + r
          j1=i1-1
          do while (j1>0 .and. dist>1)
            deltaX = maxval(abs(x(:,j1)-xx))
            dist = min(dist,deltaX)
            j1=j1-1
          end do
        end do
        if (display==1  .and. mod(i1,100)==0) print *, 'i1,xx=',i1,xx
        x(:,i1) = xx
      end do
    end subroutine rwnet
!------------------------------------------------------------
subroutine simulate_m(h,dim,n,m,numthreads,x,hmax,hmin,hmean)
  !Simulate m networks
  !Input variables -- same as in rwnet except:
  !m: Number of simulations
  !numthreads: number of threads used in parallel regions
  !Output variables
  !x: Coordinates for all m networks
  !hmax,hmin,hmean: max,min, and average final network heights
  implicit none
  integer, intent(in) :: h,dim,n,m,numthreads
  integer, dimension(dim,n,m), intent(out) ::  x
  integer, intent(out) :: hmax,hmin
  real(kind=8), intent(out) :: hmean


end subroutine simulate_m


!---------------------------
subroutine random_int(m,r)
  !Generate m random numbers each of which is 0 or 1
  !with equal probability and return numbers in array, r
  implicit none
  integer, intent(in) :: m
  integer, dimension(m), intent(out) :: r
  integer :: i1
  real(kind=8),dimension(m) :: rtemp

  call random_number(rtemp)
  r = 0
  do i1 = 1,m
    if (rtemp(i1)>=0.5) r(i1) = 1
  end do

end subroutine random_int

end module hw3
